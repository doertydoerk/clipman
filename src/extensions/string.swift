//
//  string.swift
//  clipMan
//
//  Created by Dirk Gerretz on 24.10.20.
//

extension String {
    func truncate(length: Int) -> String {
        if count <= length {
            return self
        }
        return String(prefix(length)) + "..."
    }
}
