import AppKit

protocol PasteboardListenerDelegate {
    func pasteboardDidChange()
}

class PasteboardListener {
    typealias Hook = (String) -> Void
    private let pasteboard = NSPasteboard.general
    private let timerInterval = 1.0
    private var changeCount: Int
    private var hooks: [Hook]
    var delegate: PasteboardListenerDelegate?

    init() {
        changeCount = pasteboard.changeCount
        hooks = []
    }

    func startListening() {
        Timer.scheduledTimer(timeInterval: timerInterval,
                             target: self,
                             selector: #selector(checkForChanges),
                             userInfo: nil,
                             repeats: true)
    }

    @objc func checkForChanges() {
        guard pasteboard.changeCount != changeCount else {
            return
        }

        if let lastItem = pasteboard.string(forType: NSPasteboard.PasteboardType.string) {
            for hook in hooks {
                hook(lastItem)
            }
        }

        changeCount = pasteboard.changeCount
        delegate?.pasteboardDidChange()
    }
}
