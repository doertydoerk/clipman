import Cocoa

protocol MenuDelegate {
    func pasteRequested(text: String)
}

class MenuService {
    private let keyEquivalent = [String](arrayLiteral: "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k")
    private let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
    private let popover = NSPopover()
    private var eventMonitor: EventMonitor?
    private let titleLength = 25
    var delegate: MenuDelegate?

    init() {
        configureEventMonitor()

        if let button = statusItem.button {
            button.image = NSImage(named: NSImage.Name("MenuBarIcon"))
            statusItem.menu = constructMenu(items: [String]())
        }

        popover.contentViewController = PreferencesViewController.freshController()
    }

    func updateMenuItems(items: [String]) {
        statusItem.menu = constructMenu(items: items)
    }

    func showMenu() {
        statusItem.button?.accessibilityPerformPress()
    }

    func showPopover(sender _: Any?) {
        if let button = statusItem.button {
            popover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
            eventMonitor?.start()
        }
    }

    private func closePopover(sender: Any?) {
        popover.performClose(sender)
        eventMonitor?.stop()
    }

    private func constructMenu(items: [String]) -> NSMenu {
        let menu = NSMenu()
        for (index, item) in items.enumerated() {
            if index < 10 {
                menu.addItem(constructMenuItem(item, #selector(pasteRequested(_:)), "\(index)"))
            } else {
                menu.addItem(constructMenuItem(item, #selector(pasteRequested(_:)), keyEquivalent[index - 10]))
            }
        }

        menu.addItem(NSMenuItem.separator())
//        menu.addItem(constructMenuItem("Preferences...", #selector(showPrefernces(_:)), ","))
        menu.addItem(NSMenuItem(title: "Quit", action: #selector(NSApplication.terminate(_:)), keyEquivalent: "q"))

        return menu
    }

    private func constructMenuItem(_ title: String, _ action: Selector?, _ keyEquivalent: String) -> NSMenuItem {
        let item = NSMenuItem(title: title.truncate(length: titleLength), action: action, keyEquivalent: keyEquivalent)
        item.target = self
        item.setAccessibilityTitle(title)
        return item
    }

    @objc private func pasteRequested(_ sender: NSMenuItem) {
        delegate?.pasteRequested(text: sender.accessibilityTitle()!)
    }

    @objc private func showPrefernces(_ sender: NSMenuItem) {
        print(sender.title)
    }

    private func configureEventMonitor() {
        eventMonitor = EventMonitor(mask: [.leftMouseDown, .rightMouseDown]) { [weak self] event in
            if let strongSelf = self, strongSelf.popover.isShown {
                strongSelf.closePopover(sender: event)
            }
        }
    }
}
