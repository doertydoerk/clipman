import Cocoa

class PreferencesViewController: NSViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
}

extension PreferencesViewController {
    // MARK: Storyboard instantiation

    static func freshController() -> PreferencesViewController {
        // 1.
        let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
        // 2.
        let identifier = NSStoryboard.SceneIdentifier("PreferencesViewController")
        // 3.
        guard let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? PreferencesViewController else {
            fatalError("Why cant i find PreferencesViewController? - Check Main.storyboard")
        }
        return viewcontroller
    }
}
