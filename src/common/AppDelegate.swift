import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    var app: ClipManService?

    func applicationDidFinishLaunching(_: Notification) {
        app = ClipManService()
    }

    func applicationWillTerminate(_: Notification) {}
}
