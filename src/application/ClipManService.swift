import AppKit
import Cocoa
import HotKey

class ClipManService {
    private let defaults = UserDefaults.standard
    private let historyKey = "history"
    private var items = [String]()
    private let pasteService = PasteService(NSPasteboard.general)
    private let pasteboardListener = PasteboardListener()
    private var menuService = MenuService()
    private var hotKey: HotKey!
    private let maxItems = 20

    init() {
        // init global hot key
        hotKey = HotKey(key: .v, modifiers: [.command, .shift])
        hotKey.keyDownHandler = {
            self.menuService.showMenu()
        }

        items = defaults.object(forKey: historyKey) as? [String] ?? [String]()

        pasteboardListener.delegate = self
        pasteboardListener.startListening()

        menuService.delegate = self
        menuService.updateMenuItems(items: items)
    }

    private func paste(_ item: String) {
        pasteService.paste(text: item)
    }

    private func handlePasteboardChange() {
        guard let item = pasteService.readPasteboard() else {
            return
        }

        let trimmed = item.trimmingCharacters(in: CharacterSet(arrayLiteral: " ", "\n", "\t"))
        if trimmed == "" {
            return
        }

        if items.count > 0 {
            for (index, exists) in items.enumerated() {
                if trimmed == exists {
                    items.remove(at: index)
                    break
                }
            }
        }

        items.insert(trimmed, at: 0)

        if items.count == maxItems + 1 {
            items.remove(at: maxItems)
        }

        menuService.updateMenuItems(items: items)

        defaults.set(items, forKey: historyKey)
    }
}

extension ClipManService: MenuDelegate {
    func pasteRequested(text: String) {
        paste(text)
    }
}

extension ClipManService: PasteboardListenerDelegate {
    func pasteboardDidChange() {
        handlePasteboardChange()
    }
}
