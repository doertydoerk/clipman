#!/bin/bash
BRANCH="main"
TARGET_NAME="ClipMan"
EXPORT_PLIST_PATH="./src/common/exportOptions.plist"
TEAM_ID=${MAC_APP_TEAM_ID}

# exit on failure
set -eux

git checkout "${BRANCH}"
npm ci
npm run release # --dry-run

# set version/build in Xcode
RAW_VERSION=$(git describe --tags --abbrev=0)
VERSION=${RAW_VERSION#"v"}
BUILD=$(git rev-list --count "${BRANCH}")

xcrun agvtool new-marketing-version "${VERSION}"
xcrun agvtool new-version "${BUILD}"

# create archive
xcodebuild archive -scheme clipMan -archivePath ./release/"$VERSION"/archive/"${TARGET_NAME}".xcarchive

# app from archive
xcodebuild -exportArchive -archivePath ./release/"$VERSION"/archive/"${TARGET_NAME}".xcarchive -exportPath ./release/"$VERSION"/app -exportOptionsPlist "${EXPORT_PLIST_PATH}"
codesign -f -o runtime --timestamp -s "$TEAM_ID" ./release/"$VERSION"/app/"${TARGET_NAME}".app

# create DMG
# https://github.com/sindresorhus/create-dmg
create-dmg ./release/"$VERSION"/app/ClipMan.app ./release/"$VERSION"

# notarize - https://blog.rampatra.com/how-to-notarize-a-dmg-or-zip-file-with-the-help-of-xcode-s-notary-tool
xcrun notarytool submit "./release/$VERSION/${TARGET_NAME} $VERSION.dmg" --keychain-profile "clipMan" --wait
#xcrun notarytool log 4bb64e68-7544-47c3-820e-b5067fb31829 --keychain-profile "clipMan"

# staple
xcrun stapler staple "./release/$VERSION/${TARGET_NAME} $VERSION.dmg"

# clean up
rm -rf ./release/"$VERSION"/a*

# git push everything including tags, artifacts, changelog, ...
git add .
git commit --amend --no-edit
git push --follow-tags origin "${BRANCH}"
