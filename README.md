<html>
    <div align="center">
        <img width="256" height="256" src="https://gitlab.com/doertydoerk/clipman/-/raw/main/src/common/Assets.xcassets/AppIcon.appiconset/icon_256x256.png">
    </div>
</html>

## Clip Man
Clip Man is a basic clipboard manager for macOS 12+ written in Swift 5. 

Currently, it is keeping track of the last 20 strings copied to the clipboard. Global hot key `SHIFT+CMD+v` will open up a menu from which you can choose one of the stored strings, which will be directly pasted at the current cursor position.

As this is an early stage, lots remain to be done. Right now, both the hot key and the number of strings stored are hard coded. Feel free to fork/clone and roll your own.

## Install
Get the latest [here](https://gitlab.com/doertydoerk/clipman/-/releases).

## Planned Features
- persists history between launches
- allow configurable number of strings to keep
- make hotkeys configurable
- launch on login

## Known Issues
One of the most important features for me is not having to press `CMD+v` again after having selected what to paste. For that, go to your System Settings > Security & Privacy > Accessibility and tick the box next to ClipMan.

<html>
    <div align="center">
        <img width="260" height="260" src="https://gitlab.com/doertydoerk/clipman/-/raw/main/resources/system_settings.png">
    </div>
</html>

If you find yourself having to do `CMD+v` again, you probably have installed a new version of Clip Man. In that case, remove it from the Accessibility panel via the `-` button and add it again using the `+` button.

## Why No Unit Tests?
TL:DR: The app is small and simple, and unit tests are not (yet) that important.

Being a Go backend developer, my Swift skills are very limited. However, Swift currently seems to make it hard to allow for unit testing while maintaining best practices (such as only exposing what needs to be). After a bit of research, I decided not to unit test for the sake of not exposing private vars or APIs. All suggested solutions seem to be kind of a hack. One could easily argue the other way round, but since this is a very small pet project, I figured it is OK that way. 

If you know a way to accomplish both, I'd love to learn about it. Feel free to email me.
